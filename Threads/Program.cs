﻿using System;
using System.Diagnostics;
using System.Threading;

class Program
{
    static void Main(string[] args)
    {
        // Definindo o número de tarefas
        int numTasks = 12;

        // Definindo o número de threads
        int numThreads = 4;

        Console.WriteLine("Iniciando cálculos sequenciais...");
        Stopwatch sequentialStopwatch = new();
        sequentialStopwatch.Start();
        SequentialBatchProcessing(numTasks);
        sequentialStopwatch.Stop();
        Console.WriteLine($"Tempo total para processamento sequencial: {sequentialStopwatch.ElapsedMilliseconds} ms");

        Console.WriteLine();

        Console.WriteLine($"Iniciando cálculos com {numThreads} threads...");
        Stopwatch parallelStopwatch = new();
        parallelStopwatch.Start();
        ParallelBatchProcessing(numTasks, numThreads);
        parallelStopwatch.Stop();
        Console.WriteLine($"Tempo total para processamento paralelo: {parallelStopwatch.ElapsedMilliseconds} ms");
    }

    static void SequentialBatchProcessing(int numTasks)
    {
        for (int i = 1; i <= numTasks; i++)
        {
            Console.WriteLine($"Processando tarefa {i} sequencialmente...");
            ProcessTask(i);
        }
    }

    static void ParallelBatchProcessing(int numTasks, int numThreads)
{
    // Lista para armazenar as threads
    Thread[] threads = new Thread[numThreads];

    // Inicializando e iniciando as threads
    for (int i = 0; i < numTasks; i += numThreads)
    {
        for (int j = 0; j < numThreads && i + j < numTasks; j++)
        {
            int taskNumber = i + j + 1;
            threads[j] = new Thread(() => ProcessTask(taskNumber));
            threads[j].Start();
        }

        // Aguardando a finalização das threads
        for (int j = 0; j < numThreads && i + j < numTasks; j++)
        {
            threads[j].Join();
        }
    }
}


    static void ProcessTask(int taskNumber)
    {
        // Simulando um tempo de processamento
        Thread.Sleep(1000);

        Console.WriteLine($"Tarefa {taskNumber} concluída.");
    }
}
